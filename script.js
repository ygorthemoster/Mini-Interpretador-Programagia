var inpt, tkns;
var tree, funcs;
var current;

const constants = {
  pi: 3.14,
  e: 2.7
}

function interpretar() {
  var state = 1;
  var lex = '';

  current = 0;

  inpt = document.querySelector('textarea').value + ' ';
  tkns = [];

  while(current < inpt.length){
    curChar = inpt[current];
    switch(state){
    case 1:
      if(curChar == '+'){
        tkns.push({type:'ADD'})
        current++;
      } else if(curChar == '-'){
        tkns.push({type:'SUB'})
        current++;
      } else if(curChar == '*'){
        tkns.push({type:'MUL'})
        current++;
      } else if(curChar == '/'){
        tkns.push({type:'DIV'})
        current++;
      } else if(curChar == '%'){
        tkns.push({type:'MOD'})
        current++;
      } else if(curChar == '='){
        tkns.push({type:'ATTR'})
        current++;
      } else if(curChar == '['){
        tkns.push({type:'ROUND'})
        current++;
      } else if(curChar == '('){
        tkns.push({type:'OPENP'})
        current++;
      } else if(curChar == ')'){
        tkns.push({type:'CLOSEP'})
        current++;
      } else if(curChar == ']'){
        tkns.push({type:'CLOSEB'})
        current++;
      } else if(curChar == ','){
        tkns.push({type:'COMMA'})
        current++;
      } else if(curChar == '^'){
        state = 2;
        current++;
      } else if(curChar == 'v'){
        state = 3;
        current++;
      } else if(curChar <= '9' && curChar >= '0'){
        state = 4;
        lex = curChar;
        current++;
      } else if(curChar <= 'z' && curChar >= 'a'){
        state = 5;
        lex = curChar;
        current++;
      } else if(curChar != ' '){
        return;
      } else {
      	current++;
      }
      break;
    case 2:
      if(curChar == '['){
        tkns.push({type:'CEIL'});
        state = 1;
        current++;
      } else {
        tkns.push({type:'POW'})
        state = 1;
      }
      break;
    case 3:
      if(curChar == '['){
        tkns.push({type:'FLOOR'})
        state = 1;
        current++;
      } else {
        lex = 'v';
        state = 5;
      }
      break;
    case 4:
      if(curChar <= '9' && curChar >= '0'){
        lex += curChar;
        current++;
      } else if(curChar == '.'){
        lex += curChar;
        state = 6;
        current++;
      } else {
        tkns.push({type:'NUM', val:parseFloat(lex)})
        state = 1;
      }
      break;
    case 5:
      if(curChar <= 'z' && curChar >= 'a'){
        lex += curChar;
        current++;
      } else {
        tkns.push({type:'ID', val:lex})
        state = 1;
      }
      break;
    case 6:
      if(curChar <= '9' && curChar >= '0'){
        lex += curChar;
        current++;
      } else {
        tkns.push({type:'NUM', val:parseFloat(lex)})
        state = 1;
      }
      break;
    }
  }

  document.getElementById('tokens').innerText = '';
  for (var i = 0; i < tkns.length; i++) {
    document.getElementById('tokens').innerText += `<${tkns[i].type}${tkns[i].val == undefined ? '': `, ${tkns[i].val}`}> `
  }

  tkns.push({type:'END'});
  programa();

  document.getElementById('arvore').innerText = JSON.stringify(tree, null, 2);

  document.getElementById('resultado').innerText = '';
  Run(tree, {});
}

function programa(){

  funcs = {};
  current = 0;
  var comandos = lista_comandos();
	tree = {comandos};
}

function lista_comandos(){

	var cmd = comando();
  cmd = [cmd];

  if(tkns[current].type == 'COMMA'){
    current++;
    var nxt = lista_comandos();
    cmd = cmd.concat(nxt);
  }

  return cmd;
}

function comando(){

	var aux = current;
  var tipo = 0;
  var attr = 1;

  var inBrackets = false;

  while(tkns[aux].type != 'END' && (inBrackets || tkns[aux].type != 'COMMA')){
    if(tkns[aux].type == 'ATTR') tipo = Math.max(tipo, attr);

    if(tkns[aux].type == 'OPENP' && !inBrackets){
    	inBrackets = true;
    	attr = 2;
    }
    if(tkns[aux].type == 'CLOSEP' && inBrackets) inBrackets = false;

    aux++;
  }

  switch (tipo) {
    case 0:
      return {action:'PRINT', expr:expressao()}
      break;
    case 1:
      var dest = id()
      if(tkns[current].type == 'ATTR'){
        current++;
        return {action:'ATTR', dest, expr:expressao()}
      } else {
        return;
      }
      break;
    case 2:
      var f = funcao()
      if(tkns[current].type == 'ATTR'){
        current++;
      } else {
        return;
      }

      funcs[f.id.id] = {}
      funcs[f.id.id].param = f.param
      funcs[f.id.id].def = expressao()
      return null
      break;
  }
}

function id(){

	if(tkns[current].type == 'ID'){
    var ID = tkns[current].val;
    current++;
    return {action: 'VAR', id: ID};
  } else {
    return;
  }
}

function num(){

  if(tkns[current].type == 'NUM'){
    var ID = tkns[current].val;
    current++;
    return {action: 'NUM', id: ID};
  } else {
    return;
  }
}

function funcao(){

	var ID = id();
  if(tkns[current].type == 'OPENP'){
    current++;
  } else {
    return;
  }
  var param = parametros();
  if(tkns[current].type == 'CLOSEP'){

    current++;
  } else {
    return;
  }


  return {action:'CALL', id: ID, param};
}

function parametros(){

	var ids = id();
  ids = [ids];

  if(tkns[current].type == 'COMMA'){
  	current++;
    var nxt = parametros();
    ids = ids.concat(nxt);
  }

  return ids;
}

function expressao(){

	var parc = parcela();
  if(tkns[current].type == 'ADD'){
  	current++;
    var nxt = expressao();
    return {action:'+', left: parc, right: nxt}
  } else if(tkns[current].type == 'SUB'){
  	current++;
    var nxt = expressao();
    return {action:'-', left: parc, right: nxt}
  }
  return parc;
}

function parcela(){

	var fat = fator();
  if(tkns[current].type == 'MUL'){
  	current++;
    var nxt = parcela();
    return {action:'*', left: fat, right: nxt}
  } else if(tkns[current].type == 'DIV'){
  	current++;
    var nxt = parcela();
    return {action:'/', left: fat, right: nxt}
  } else if(tkns[current].type == 'MOD'){
  	current++;
    var nxt = parcela();
    return {action:'%', left: fat, right: nxt}
  }
  return fat
}

function fator(){

  var pot = potencia();
  if(tkns[current].type == 'POW'){
  	current++;
    var nxt = potencia();
    return {action:'^', left: pot, right: nxt}
  }
  return pot;
}

function potencia(){

	if(tkns[current].type == 'OPENP'){
    current++;
    var expr = expressao();
    if(tkns[current].type == 'CLOSEP'){
    	current++;
 		} else {
      return;
    }
    return expr;
  } else if(tkns[current].type == 'ROUND'){
    current++;
    var expr = expressao();
    if(tkns[current].type == 'CLOSEB'){
    	current++;
 		} else {
      return;
    }
    return {action:'ROUND', expr}
  } else if(tkns[current].type == 'CEIL'){
    current++;
    var expr = expressao();
    if(tkns[current].type == 'CLOSEB'){
    	current++;
 		} else {
      return;
    }
    return {action:'CEIL', expr}
	} else if(tkns[current].type == 'FLOOR'){
    current++;
    var expr = expressao();
    if(tkns[current].type == 'CLOSEB'){
    	current++;
 		} else {
      return;
    }
    return {action:'FLOOR', expr}
	} else {
    if(tkns[current].type == 'NUM') return num();
    else if(tkns[current].type == 'ID'){
    	if(tkns[current + 1].type == 'OPENP'){
      	return funcao();
      } else {
      	return id();
      }
    } else {
      return;
    }
  }
}

function Run(node, variables) {
  if(node == null){
    return {result:null, variables}
  } else if(node.comandos != undefined){
    for (var i = 0; i < node.comandos.length; i++) {
      var run = Run(node.comandos[i], variables);
      variables = run.variables;
    }
  } else {
    switch (node.action) {
      case 'PRINT':
        var run = Run(node.expr, variables);
        variables = run.variables;
        document.getElementById('resultado').innerText += '\n' + JSON.stringify(run.result);
        return {result: null, variables}
        break;
      case 'ATTR':
        var run = Run(node.expr, variables);
        variables = run.variables;
        variables[node.dest.id] = run.result;
        return {result: null, variables}
        break;
      case '+':
        var l = Run(node.left, variables);
        variables = l.variables;
        var r = Run(node.right, variables);
        variables = r.variables;
        return {result: l.result + r.result, variables}
        break;
      case '-':
        var l = Run(node.left, variables);
        variables = l.variables;
        var r = Run(node.right, variables);
        variables = r.variables;
        return {result: l.result - r.result, variables}
        break;
      case '*':
        var l = Run(node.left, variables);
        variables = l.variables;
        var r = Run(node.right, variables);
        variables = r.variables;
        return {result: l.result * r.result, variables}
        break;
      case '/':
        var l = Run(node.left, variables);
        variables = l.variables;
        var r = Run(node.right, variables);
        variables = r.variables;
        return {result: l.result / r.result, variables}
        break;
      case '%':
        var l = Run(node.left, variables);
        variables = l.variables;
        var r = Run(node.right, variables);
        variables = r.variables;
        return {result: parseInt(l.result) % parseInt(r.result), variables}
        break;
      case '^':
        var l = Run(node.left, variables);
        variables = l.variables;
        var r = Run(node.right, variables);
        variables = r.variables;
        return {result: Math.pow(l.result, r.result), variables}
        break;
      case 'ROUND':
        var run = Run(node.expr, variables);
        variables = run.variables;
        return {result: Math.round(run.result), variables}
        break;
      case 'CEIL':
        var run = Run(node.expr, variables);
        variables = run.variables;
        return {result: Math.ceil(run.result), variables}
        break;
      case 'FLOOR':
        var run = Run(node.expr, variables);
        variables = run.variables;
        return {result: Math.floor(run.result), variables}
        break;
      case 'VAR':
        console.log(variables);
        if(variables[node.id] == undefined) return {result: constants[node.id], variables}
        else return {result: variables[node.id], variables}
        break;
      case 'NUM':
        return {result: node.id, variables}
        break;
      case 'CALL':
        let p = {}
        for (var i = 0; i < funcs[node.id.id].param.length; i++) {
          p[funcs[node.id.id].param[i].id] = variables[node.param[i].id];
        }
        let f = Run(funcs[node.id.id].def, p);
        return {result: f.result, variables}
        break;
    }
  }
}
